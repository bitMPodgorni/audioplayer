//
//  AccountTVC.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 10/03/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class AccountTVC: UITableViewController {

    @IBOutlet weak var userEmailTxt: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userEmailTxt.text = UserDefaults.standard.string(forKey: "userEmail")
    }
}
