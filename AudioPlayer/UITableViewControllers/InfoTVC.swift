//
//  SettingsTVC.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 10/03/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit
import SafariServices

class InfoTVC: UITableViewController, SFSafariViewControllerDelegate {
    
    @IBOutlet weak var thisAppBuildNumber: UILabel!
    
    var masterVC = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if let nsObject = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject? {
            if let version = nsObject as? String {
                thisAppBuildNumber.text = "beta \(version)"
            }
        }
        
    }

    @IBAction func safariButtonPressed(_ sender: Any) {
        let urlString = "http://www.mpodgorni.pl"
        
        if let url = URL(string: urlString) {
            let safariVC = SFSafariViewController(url: url)
            safariVC.delegate = self
            UIApplication.shared.keyWindow?.rootViewController?.present(safariVC, animated: true)
        }
    }
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        guard let tempAudioPlayer = MediaPlayerVC.shared as? MediaPlayerVC else { return }
        tempAudioPlayer.stopButton()
        LoggerSwitcher.updateRootVC()
    }
}
