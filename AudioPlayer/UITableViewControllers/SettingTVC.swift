//
//  SettingTVC.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 10/03/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class SettingTVC: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let selectedRow = tableView.indexPathForSelectedRow {
            let deselectedRow = tableView.cellForRow(at: selectedRow)
            UIView.animate(withDuration: 0.3) {
                deselectedRow!.backgroundColor = color_light_black
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedRow = tableView.cellForRow(at: indexPath)
        selectedRow!.backgroundColor = color_dark_orange
    }
    
}
