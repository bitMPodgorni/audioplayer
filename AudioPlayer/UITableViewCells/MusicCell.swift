//
//  MusicCell.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 28/02/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class MusicCell: UITableViewCell {

    @IBOutlet weak var musicCoverImage: UIImageView!
    @IBOutlet weak var musicTitle: UITextView!
    @IBOutlet weak var musicArtist: UITextView!
    
    func setMusic(music: Music) {
        musicCoverImage.image = music.image
        musicTitle.text = music.title
        musicArtist.text = music.artist
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if(selected) {
            musicTitle.textColor = color_dark_orange
        } else {
            musicTitle.textColor = .white
        }
    }
}
