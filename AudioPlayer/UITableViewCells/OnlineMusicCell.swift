//
//  OnlineMusicCell.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 23/05/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class OnlineMusicCell: UITableViewCell {

    @IBOutlet weak var musicCoverImage: UIImageView!
    @IBOutlet weak var musicTitle: UITextView!
    @IBOutlet weak var musicArtist: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if(selected) {
            musicTitle.textColor = color_dark_orange
        } else {
            musicTitle.textColor = .white
        }
    }
}
