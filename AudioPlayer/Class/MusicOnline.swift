//
//  MusicOnline.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 22/05/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import Foundation
import UIKit

class MusicOnline: Equatable {

    var imieAutora: String
    var nazwiskoAutora: String
    var nazwaZespolu: String
    var lokalizacjaPliku: String
    var lokalizacjaIkonaDuza: UIImage
    
    init(imieAutora: String, nazwiskoAutora: String, nazwaZespolu: String, lokalizacjaPliku: String, lokalizacjaIkonaDuza: UIImage) {
        self.imieAutora = imieAutora
        self.nazwiskoAutora = nazwiskoAutora
        self.nazwaZespolu = nazwaZespolu
        self.lokalizacjaPliku = lokalizacjaPliku
        self.lokalizacjaIkonaDuza = lokalizacjaIkonaDuza
    }
    
    static func == (lhs: MusicOnline, rhs: MusicOnline) -> Bool {
        return lhs.imieAutora == rhs.imieAutora &&
        lhs.nazwiskoAutora == rhs.nazwiskoAutora &&
        lhs.nazwaZespolu == rhs.nazwaZespolu &&
        lhs.lokalizacjaPliku == rhs.lokalizacjaPliku &&
        lhs.lokalizacjaIkonaDuza == rhs.lokalizacjaIkonaDuza
    }
}
