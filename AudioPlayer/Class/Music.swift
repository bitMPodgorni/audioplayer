//
//  Music.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 28/02/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import Foundation
import UIKit

class Music: Equatable {
    var fileName: String
    var fileExtension: String
    var image: UIImage
    var title: String
    var artist: String
    
    init(fileName: String, fileExtension: String, image: UIImage, title: String, artist: String) {
        self.fileName = fileName
        self.fileExtension = fileExtension
        self.image = image
        self.title = title
        self.artist = artist
    }
    
    static func == (lhs: Music, rhs: Music) -> Bool {
        return lhs.fileName == rhs.fileName && lhs.fileExtension == rhs.fileExtension && lhs.image == rhs.image && lhs.title == rhs.title && lhs.artist == rhs.artist
    }
}
