//
//  mainDesignableSlider.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 14/02/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

@IBDesignable
class MainSlider: UISlider {

    @IBInspectable var trackHeight: CGFloat = 6
    @IBInspectable var thumbImage: UIImage? {
        didSet {
            setThumbImage(thumbImage, for: .normal)
        }
    }
    @IBInspectable var highlightedImage: UIImage? {
        didSet {
            setThumbImage(highlightedImage, for: .highlighted)
        }
    }
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: bounds.origin, size: CGSize(width: bounds.width, height: trackHeight))
    }
}
