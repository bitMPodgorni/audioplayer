//
//  Switcher.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 01/05/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import Foundation
import UIKit

class LoggerSwitcher {
    static func updateRootVC() {
        let status = UserDefaults.standard.bool(forKey: "isLoggedIn")
        var rootVC: UIViewController?
        
        if status {
            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MasterVC") as! MasterVC
        } else {
            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoggerVC") as! LoggerVC
        }
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        appDelegate.window?.rootViewController = rootVC
    }
}
