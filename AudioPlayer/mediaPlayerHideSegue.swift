//
//  mediaPlayerShowSegue.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 24/03/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class mediaPlayerHideSegue: UIStoryboardSegue {

    override func perform() {
        guard let from = self.source.view as UIView? else {return}
        guard let to = self.destination.view as UIView? else {return}
        let toVC = self.destination
        
        let containerView = from.superview
        
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        let newScreenHeight = screenHeight - 130
        
        from.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        to.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        
//        let window = UIApplication.shared.keyWindow
//        window?.insertSubview(from, aboveSubview: to)
        containerView?.insertSubview(to, at: 0)
//        containerView?.addSubview(to)
        
        
        UIView.animate(withDuration: 0.3, animations: {
            from.frame = from.frame.offsetBy(dx: 0.0, dy: newScreenHeight)
            to.frame = to.frame.offsetBy(dx: 0.0, dy: 0.0)
        }) { (success) in
            self.source.dismiss(animated: false, completion: nil)
        }
    }
    
}
