//
//  VariableNotificationCenter.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 28/02/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import Foundation
import UIKit

let duration_for_slide_media_player = 0.3
let duration_for_toggle_media_player_animation = 0.2

let color_dark_black = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
let color_light_black = #colorLiteral(red: 0.1298420429, green: 0.1298461258, blue: 0.1298439503, alpha: 1)
let color_dark_orange = #colorLiteral(red: 1, green: 0.8784313725, blue: 0, alpha: 1)
let color_light_orange = #colorLiteral(red: 1, green: 0.8784313725, blue: 0.3333333333, alpha: 1)
let color_white = UIColor.white
let color_gray = UIColor.gray
