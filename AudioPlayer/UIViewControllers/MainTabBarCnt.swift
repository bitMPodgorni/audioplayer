//
//  MainTabBarCnt.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 28/03/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class MainTabBarCnt: UITabBarController {
    
//    var masterVCInstantiatse: MasterVC = MasterVC()
    
    var playlistsVC = UIViewController()
    var onlineListVC = UIViewController()
    var masterNVC = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBar.isHidden = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        playlistsVC = storyboard.instantiateViewController(withIdentifier: "PlaylistVC")
        onlineListVC = storyboard.instantiateViewController(withIdentifier: "OnlineListVC")
        masterNVC = storyboard.instantiateViewController(withIdentifier: "MasterNVC")
        
        self.viewControllers = [playlistsVC, onlineListVC, masterNVC]
    }
}
