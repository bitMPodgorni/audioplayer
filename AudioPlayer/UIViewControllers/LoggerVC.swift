//
//  LoggerVC.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 05/03/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class LoggerVC: UIViewController {
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    
    static var shared = UIViewController()
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        
        loginBtn.layer.cornerRadius = 30
        loginBtn.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        registerBtn.layer.cornerRadius = 30
        registerBtn.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        createGradientLayer()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LoggerVC.shared = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let isLoggedIn = UserDefaults.standard.bool(forKey: "isLoggedIn")
        if(isLoggedIn) {
            dismiss(animated: true, completion: nil)
        }
    }
    
    func createGradientLayer() {
        view.backgroundColor = UIColor.clear
        var backgroundLayer: CAGradientLayer!
        backgroundLayer = CAGradientLayer()
        backgroundLayer.frame = view.frame
//        backgroundLayer.colors = [UIColor.black.cgColor, UIColor.yellow.cgColor]
        backgroundLayer.colors = [UIColor.black.cgColor, UIColor(red: 255.0/255.0, green: 224.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor]
//        backgroundLayer.startPoint = CGPoint(x: 0, y: 0)
//        backgroundLayer.endPoint = CGPoint(x:1, y:1)
        view.layer.insertSublayer(backgroundLayer, at: 0)
    }
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        guard let registerVC = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") else { return }
        present(registerVC, animated: true, completion: nil)
    }
    @IBAction func loginButtonPressed(_ sender: Any) {
        guard let logginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") else { return }
        present(logginVC, animated: true, completion: nil)
    }
}
