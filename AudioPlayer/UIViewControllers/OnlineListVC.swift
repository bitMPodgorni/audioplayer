//
//  OnlineListVC.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 23/05/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class OnlineListVC: UIViewController {

    @IBOutlet var musicTableView: UITableView!
    @IBOutlet weak var loadingActivIndicator: UIActivityIndicatorView!
    
    static var shared = UIViewController()
    
    var selectedIndexPath: IndexPath?
    
    let URL_USER_LISTS = "https://wsbaplikacja.prv.pl/wsbapi/lista.php"
    
    var musicsOnline = [MusicOnline]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        OnlineListVC.shared = self
        
        jsonMyFunc()
        
        musicTableView.delegate = self
        musicTableView.dataSource = self
    }
    
    func jsonMyFunc(){
        let url = URL(string: URL_USER_LISTS)
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let data = data else { return }
            do {
                DispatchQueue.main.sync {
                    UIApplication.shared.beginIgnoringInteractionEvents()
                    self.musicsOnline.removeAll()
                    self.musicTableView.reloadData()
                    self.loadingActivIndicator.isHidden = false
                    self.loadingActivIndicator.startAnimating()
                }
                var dataAsString = String(data: data, encoding: .utf8)
                dataAsString = dataAsString?.replacingOccurrences(of: "<!-- End //]]>'\"</script></style></iframe></noembed></embed></object></noscript>--><div id=\"prv_main_link\"><a href=\"https://www.prv.pl\" title=\"darmowy hosting\">Darmowy hosting</a> zapewnia PRV.PL</div><script type=\"text/javascript\" src=\"/prv_site_config_values.js\"></script><script type=\"text/javascript\" src=\"/prv_hosting_footer.js\"></script>", with: "")
                let myData = dataAsString!.data(using: .utf8)!
                
                guard let json = try JSONSerialization.jsonObject(with: myData, options: []) as? [[String: Any]] else { return }
                for dic in json {
                    guard let imieAutora = dic["imie_autora"] as? String else { return }
                    guard let nazwiskoAutora = dic["nazwisko_autora"] as? String else { return }
                    guard let nazwaZespolu = dic["nazwa_zespolu"] as? String else { return }
                    guard let lokalizacjaPliku = dic["lokalizacja_mp3"] as? String else { return }
                    guard let lokalizacjaIkonaDuza = dic["lokalizacja_ikona_duza"] as? String else { return }
                    
                    if let imageURL = NSURL(string: lokalizacjaIkonaDuza) {
                        if let data = NSData(contentsOf: (imageURL as URL)) {
                            if let lokalizacjaIkonaDuzaImage = UIImage(data: data as Data) {
                                self.musicsOnline.append(MusicOnline(imieAutora: imieAutora, nazwiskoAutora: nazwiskoAutora, nazwaZespolu: nazwaZespolu, lokalizacjaPliku: lokalizacjaPliku, lokalizacjaIkonaDuza: lokalizacjaIkonaDuzaImage))
                            }
                        }
                    } else {
                        print("nie działa")
                    }
                }
                DispatchQueue.main.sync {
                    self.loadingActivIndicator.stopAnimating()
                    self.loadingActivIndicator.isHidden = true
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
                DispatchQueue.main.async {
                    self.musicTableView.reloadData()
                }
            } catch let err {
                print(err.localizedDescription)
            }
        }.resume()
        
    }
}
extension OnlineListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return musicsOnline.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OnlineMusicCell") as! OnlineMusicCell
        
        cell.musicArtist.text = musicsOnline[indexPath.row].imieAutora
        cell.musicTitle.text = musicsOnline[indexPath.row].nazwaZespolu
        cell.musicCoverImage.image = musicsOnline[indexPath.row].lokalizacjaIkonaDuza
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let tempMediaPlayerVC = MediaPlayerVC.shared as? MediaPlayerVC {
            if(tempMediaPlayerVC.playlistPlayingOnline == musicsOnline ) {
                tempMediaPlayerVC.playSoundOnline(tempMediaPlayerVC.playlistPlayingOnline[indexPath.row])
                tempMediaPlayerVC.rowPlaying = indexPath.row
            } else {
                tempMediaPlayerVC.playSoundOnlineWith(music: musicsOnline[indexPath.row], musics: musicsOnline, row: indexPath.row)
            }
        }
        
        selectedIndexPath = indexPath
        
        if let tempMasterVC = MasterVC.shared as? MasterVC {
            tempMasterVC.firstMiniPlayerShow()
        }
    }
}
extension OnlineListVC {
    func scrollToTop() {
        let indexPath = IndexPath(row: 0, section: 0)
        musicTableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
}
