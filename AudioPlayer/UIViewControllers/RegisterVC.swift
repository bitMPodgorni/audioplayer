//
//  RegisterVC.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 22/03/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController, UITextFieldDelegate {
    
    static var shared = UIViewController()
    
    @IBOutlet weak var messageFromBtnLabel: UILabel!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var rePasswordTxtField: UITextField!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var zarejestrujBtn: UIButton!
    
    let URL_USER_REGISTER = "https://wsbaplikacja.prv.pl/wsbapi/register_ios.php"
    
    override func viewWillAppear(_ animated: Bool) {
        createGradientLayer()
        closeBtn.layer.cornerRadius = 17
        zarejestrujBtn.layer.cornerRadius = 10
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RegisterVC.shared = self
        
        UIApplication.shared.statusBarStyle = .default
        emailTxtField.delegate = self
        passwordTxtField.delegate = self
        
        self.hideKeyboardWhenTappedAround()
    }
    
    func createGradientLayer() {
        view.backgroundColor = UIColor.clear
        var backgroundLayer: CAGradientLayer!
        backgroundLayer = CAGradientLayer()
        backgroundLayer.frame = view.frame
        let colorTop = UIColor(red: 164.0 / 255.0, green: 179.0 / 255.0, blue: 87.0 / 255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 117.0 / 255.0, green: 137.0 / 255.0, blue: 12.0 / 255.0, alpha: 1.0).cgColor
        backgroundLayer.colors = [colorTop, colorBottom]
        backgroundLayer.startPoint = CGPoint(x: 0, y: 0)
        backgroundLayer.endPoint = CGPoint(x:1, y:1)
        view.layer.insertSublayer(backgroundLayer, at: 0)
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func regButtonPressed(_ sender: Any) {
        messageFromBtnLabel.text = ""
        guard let email = emailTxtField.text else { return }
        guard let password = passwordTxtField.text else { return }
        guard let rePassword = rePasswordTxtField.text else { return }
        
        var emailBool = false
        var hasloBool = false
        
        if(email.count >= 6) {
            if(email.contains("@") && email.contains(".")) {
                emailBool = true
            } else {
                messageFromBtnLabel.text = "Adres email jest niepoprawny."
            }
        } else {
            messageFromBtnLabel.text = "Niepoprawny adres email! Minimum 6 znaków."
        }
        
        if(emailBool && password == rePassword) {
            if(password.count >= 8) {
                hasloBool = true
            } else {
                messageFromBtnLabel.text = "Hasło jest zbyt krótkie. Minimum 8 znaków."
            }
        } else if(emailBool && password != rePassword) {
            messageFromBtnLabel.text = "Hasła nie są identyczne!"
        }
        
        if(emailBool && hasloBool) {
            let headers = [
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            let postData = NSMutableData(data: "user_name=null&user_email=\(email)&user_pass=\(password)".data(using: String.Encoding.utf8)!)
            let request = NSMutableURLRequest(url: NSURL(string: URL_USER_REGISTER)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
            
            
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            URLSession.shared.dataTask(with: request as URLRequest) { (data, response, err) in
                guard let data = data else { return }
                let dataAsString = String(data: data, encoding: .utf8)
                print(dataAsString)
                if(dataAsString?.prefix(1) == "0") {
                    print("niezarejestrowano")
                    DispatchQueue.main.async {
                        self.messageFromBtnLabel.text = "Niepoprawne dane! Popraw wprowadzone dane i spróbuj ponownie."
                    }
                } else if(dataAsString?.prefix(1) == "1") {
                    print("zarejestrowano")
                    DispatchQueue.main.async {
                        UserDefaults.standard.set(true, forKey: "isLoggedIn")
                        UserDefaults.standard.set("\(email)", forKey: "userEmail")
                        LoggerSwitcher.updateRootVC()
                    }
                }
                }.resume()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == emailTxtField) {
            passwordTxtField.becomeFirstResponder()
        } else if(textField == passwordTxtField) {
            rePasswordTxtField.becomeFirstResponder()
        } else if(textField == rePasswordTxtField) {
            dismissKeyboard()
            regButtonPressed(self)
        }
        return true
    }
    
}

extension RegisterVC {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterVC.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
