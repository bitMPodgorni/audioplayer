//
//  MediaPlayerVC.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 10/02/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit
import MediaPlayer

class MediaPlayerVC: UIViewController {
    
    // Segues
    var masterVCInstantiate: MasterVC = MasterVC()
    var mainTabBarCntInstantiate: MainTabBarCnt = MainTabBarCnt()
    
    // IBOutles
    @IBOutlet weak var miniPlayerView: UIView!
    @IBOutlet weak var valueOfSlide: MainSlider!
    @IBOutlet weak var valueOfSlideMiniPlayer: MiniPlayerSlider!
    @IBOutlet weak var passedTimeTextView: UITextView!
    @IBOutlet weak var remainedTimeTextView: UITextView!
    
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var coverImageMiniPlayer: UIImageView!
    @IBOutlet weak var defaultCoverBlur: UIImageView!
    @IBOutlet weak var coverShadowImage: UIImageView!
    
    @IBOutlet weak var titleOfMusicTextView: UITextView!
    @IBOutlet weak var titleOfMusicTextViewMiniPlayer: UITextView!
    @IBOutlet weak var artistOfMusicTextView: UITextView!
    @IBOutlet weak var artistOfMusicTextViewMiniPlayer: UITextView!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playButtonMiniPlayer: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var pauseButtonMiniPlayer: UIButton!
    @IBOutlet weak var repeatButton: UIButton!
    @IBOutlet weak var shuffleButton: UIButton!
    var repeatButtonValue = 0
    var shuffleButtonValue = 0
    
    static var shared = UIViewController()
    
    // Music Pass
    var playlistPlaying: [Music] = []

    var playlistPlayingOnline: [MusicOnline] = []
    
    var rowPlaying: Int?
    
    // Var & Let
//    var audioSourceURL: URL!
    var updateSliderTime: Timer = Timer()
    var currentTime: Double = 0.0
    var titleMusic = ""
    var artistMusic = ""

    var isOnTopMiniPlayer = false
    
    // OVERRIDE viewDidLoad()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MediaPlayerVC.shared = self
        
        repeatButtonValue = UserDefaults.standard.integer(forKey: "repeatButtonValue")
        switchInRepeatButtonValue()
        shuffleButtonValue = UserDefaults.standard.integer(forKey: "shuffleButtonValue")
        switchInSuffleButtonValue()
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        blurEffectInBackground()
        
        valueOfSlide.setValue(valueOfSlide.minimumValue, animated: false)
        valueOfSlideMiniPlayer.setValue(valueOfSlide.minimumValue, animated: false)
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            print("Playback OK")
            try AVAudioSession.sharedInstance().setActive(true)
            print("Session is Active")
        } catch {
            print(error)
        }
    }
    
    override var canBecomeFirstResponder: Bool{ get{return true} }
    
    override var canResignFirstResponder: Bool{ get{return true} }
    
    override func remoteControlReceived(with event: UIEvent?) {
        if let event = event {
            if event.type == .remoteControl {
                switch event.subtype {
                case .remoteControlPlay:
                    playButton(self)
                case .remoteControlStop:
                    stopButton()
                case .remoteControlPause:
                    pauseButton(self)
                case .remoteControlTogglePlayPause:
                    print("remoteControlTogglePlayPause")
                    if (AudioPlayer.shared.player.isPlaying) {
                        AudioPlayer.shared.pause()
                    } else {
                        AudioPlayer.shared.play()
                    }
                case .remoteControlNextTrack:
                    nextTrackButtonPressed(self)
                case .remoteControlPreviousTrack:
                    backTrackButtonPressed(self)
                case .remoteControlBeginSeekingForward:
                    print("BeginSeekingForward")
                case .remoteControlBeginSeekingBackward:
                    print("BeginSeekingBackward")
                default:
                    break
                }
            }
        }
    }
    
    // Creating Function
    func playSoundWith(music: Music, musics: [Music], row: Int) {
        updateSliderTime.invalidate()
        
        if let tempOnlineListVC = mainTabBarCntInstantiate.onlineListVC as? OnlineListVC  {
            if (AudioPlayer.shared.audioOnlineURL != nil) {
                let indexPathDeselect = IndexPath(row: rowPlaying!, section: 0)
                tempOnlineListVC.musicTableView.deselectRow(at: indexPathDeselect, animated: true)
                AudioPlayer.shared.stop()
                AudioPlayer.shared.audioOnlineURL = nil
            }
        }
        
        let musicId = music
        
        playlistPlaying = musics
        
        rowPlaying = row
        
        titleMusic = musicId.title
        artistMusic = musicId.artist
        coverImage.image = musicId.image
        coverImageMiniPlayer.image = musicId.image
        defaultCoverBlur.image = musicId.image
        
        titleOfMusicTextView.text = musicId.title//titleMusic
        titleOfMusicTextViewMiniPlayer.text = musicId.title//titleMusic
        artistOfMusicTextView.text = musicId.artist//artistMusic
        artistOfMusicTextViewMiniPlayer.text = musicId.artist//artistMusic
        
        
        AudioPlayer.shared.playSoundWith(name: musicId.fileName, ext: musicId.fileExtension)
        AudioPlayer.shared.player.currentTime = AudioPlayer.shared.player.currentTime * AudioPlayer.shared.player.duration
        
        updateSliderTime = updateSliderTimeStart()
        
        imageCoverIsPlaying()
        
        playButton.isHidden = true
        playButtonMiniPlayer.isHidden = true
        pauseButton.isHidden = false
        pauseButtonMiniPlayer.isHidden = false
        
        updateNowPlayingInfoCenter(musicId.image)
    }
    func playSound(_ musicId: Music) -> Void {
        updateSliderTime.invalidate()
        
        if let tempOnlineListVC = mainTabBarCntInstantiate.onlineListVC as? OnlineListVC {
            if (AudioPlayer.shared.audioOnlineURL != nil) {
                let indexPathDeselect = IndexPath(row: rowPlaying!, section: 0)
                tempOnlineListVC.musicTableView.deselectRow(at: indexPathDeselect, animated: true)
                AudioPlayer.shared.stop()
                AudioPlayer.shared.audioOnlineURL = nil
            }
        }
        
        titleMusic = musicId.title
        artistMusic = musicId.artist
        coverImage.image = musicId.image
        coverImageMiniPlayer.image = musicId.image
        defaultCoverBlur.image = musicId.image
        
        titleOfMusicTextView.text = musicId.title//titleMusic
        titleOfMusicTextViewMiniPlayer.text = musicId.title//titleMusic
        artistOfMusicTextView.text = musicId.artist//artistMusic
        artistOfMusicTextViewMiniPlayer.text = musicId.artist//artistMusic
        
        
        AudioPlayer.shared.playSoundWith(name: musicId.fileName, ext: musicId.fileExtension)
        AudioPlayer.shared.player.currentTime = AudioPlayer.shared.player.currentTime * AudioPlayer.shared.player.duration
        
        updateSliderTime = updateSliderTimeStart()
        
        imageCoverIsPlaying()
        
        playButton.isHidden = true
        playButtonMiniPlayer.isHidden = true
        pauseButton.isHidden = false
        pauseButtonMiniPlayer.isHidden = false
        
        updateNowPlayingInfoCenter(musicId.image)
    }
    
    func playSoundOnlineWith(music: MusicOnline, musics: [MusicOnline], row: Int) {
        updateSliderTime.invalidate()
        
        if let tempPlaylistsVC = mainTabBarCntInstantiate.playlistsVC as? PlaylistsVC {
            if (AudioPlayer.shared.audioURL != nil) {
                let indexPathDeselect = IndexPath(row: rowPlaying!, section: 0)
                tempPlaylistsVC.mainTableView.deselectRow(at: indexPathDeselect, animated: true)
                AudioPlayer.shared.stop()
                AudioPlayer.shared.audioURL = nil
            }
        }
        
        let musicId = music
        
        playlistPlayingOnline = musics
        
        rowPlaying = row
        
        titleMusic = musicId.nazwaZespolu
        artistMusic = musicId.imieAutora
        coverImage.image = musicId.lokalizacjaIkonaDuza
        coverImageMiniPlayer.image = musicId.lokalizacjaIkonaDuza
        defaultCoverBlur.image = musicId.lokalizacjaIkonaDuza
        
        titleOfMusicTextView.text = musicId.nazwaZespolu//titleMusic
        titleOfMusicTextViewMiniPlayer.text = musicId.nazwaZespolu//titleMusic
        artistOfMusicTextView.text = musicId.imieAutora//artistMusic
        artistOfMusicTextViewMiniPlayer.text = musicId.imieAutora//artistMusic
        
        
        AudioPlayer.shared.playSoundOnlineWith(localized: music.lokalizacjaPliku)
//        AudioPlayer.shared.player.currentTime = AudioPlayer.shared.player.currentTime * AudioPlayer.shared.player.duration
        
        updateSliderTime = updateSliderTimeStart()
        
        imageCoverIsPlaying()
        
        playButton.isHidden = true
        playButtonMiniPlayer.isHidden = true
        pauseButton.isHidden = false
        pauseButtonMiniPlayer.isHidden = false
        
        updateNowPlayingInfoCenter(musicId.lokalizacjaIkonaDuza)
    }
    func playSoundOnline(_ musicId: MusicOnline) -> Void {
        updateSliderTime.invalidate()
        
        if let tempPlaylistsVC = mainTabBarCntInstantiate.playlistsVC as? PlaylistsVC {
            if (AudioPlayer.shared.audioURL != nil) {
                let indexPathDeselect = IndexPath(row: rowPlaying!, section: 0)
                tempPlaylistsVC.mainTableView.deselectRow(at: indexPathDeselect, animated: true)
                AudioPlayer.shared.stop()
                AudioPlayer.shared.audioURL = nil
            }
        }
        
        titleMusic = musicId.nazwaZespolu
        artistMusic = musicId.imieAutora
        coverImage.image = musicId.lokalizacjaIkonaDuza
        coverImageMiniPlayer.image = musicId.lokalizacjaIkonaDuza
        defaultCoverBlur.image = musicId.lokalizacjaIkonaDuza
        
        titleOfMusicTextView.text = musicId.nazwaZespolu//titleMusic
        titleOfMusicTextViewMiniPlayer.text = musicId.nazwaZespolu//titleMusic
        artistOfMusicTextView.text = musicId.imieAutora//artistMusic
        artistOfMusicTextViewMiniPlayer.text = musicId.imieAutora//artistMusic
        
        
        AudioPlayer.shared.playSoundOnlineWith(localized: musicId.lokalizacjaPliku)
        //AudioPlayer.shared.player.currentTime = AudioPlayer.shared.player.currentTime * AudioPlayer.shared.player.duration
        
        updateSliderTime = updateSliderTimeStart()
        
        imageCoverIsPlaying()
        
        playButton.isHidden = true
        playButtonMiniPlayer.isHidden = true
        pauseButton.isHidden = false
        pauseButtonMiniPlayer.isHidden = false
        
        updateNowPlayingInfoCenter(musicId.lokalizacjaIkonaDuza)
    }
    
    func updateSliderTimeStart() -> Timer {
        return Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.mainMusicTimer), userInfo: nil, repeats: true)
    }
    
    @objc func mainMusicTimer() {
        AudioPlayer.shared.playerOnline.automaticallyWaitsToMinimizeStalling = false
        if(AudioPlayer.shared.audioURL != nil) {
            valueOfSlide.value = Float(AudioPlayer.shared.player.currentTime/AudioPlayer.shared.player.duration)
            valueOfSlideMiniPlayer.value = Float(AudioPlayer.shared.player.currentTime/AudioPlayer.shared.player.duration)
            
            updatePassedTime(AudioPlayer.shared.player.currentTime)
            updateRemainedTime(AudioPlayer.shared.player.duration, AudioPlayer.shared.player.currentTime)
            if(true) {
                valueOfSlide.value = Float(AudioPlayer.shared.player.currentTime/AudioPlayer.shared.player.duration)
                valueOfSlideMiniPlayer.value = Float(AudioPlayer.shared.player.currentTime/AudioPlayer.shared.player.duration)
                
                updatePassedTime(AudioPlayer.shared.player.currentTime)
                updateRemainedTime(AudioPlayer.shared.player.duration, AudioPlayer.shared.player.currentTime)
                switch repeatButtonValue {
                case 0: // normal
                    if(valueOfSlide.value == 0.0 && !AudioPlayer.shared.player.isPlaying) {
                        if((playlistPlaying.count) > (rowPlaying! + 1)) {
                            playSound(playlistPlaying[rowPlaying! + 1])
                            if let sharedPlaylistsVC = mainTabBarCntInstantiate.playlistsVC as? PlaylistsVC {
                                let indexPathDeselect = IndexPath(row: rowPlaying!, section: 0)
                                sharedPlaylistsVC.mainTableView.deselectRow(at: indexPathDeselect, animated: true)
                                let indexPathSelect = IndexPath(row: rowPlaying! + 1, section: 0)
                                sharedPlaylistsVC.mainTableView.selectRow(at: indexPathSelect, animated: true, scrollPosition: .none)
                                sharedPlaylistsVC.selectedIndexPath = indexPathDeselect
                            }
                            rowPlaying! += 1
                        } else {
                            pauseButton(self)
                            updateSliderTime.invalidate()
                        }
                    }
                case 1: // repeat all
                    if(valueOfSlide.value == 0.0 && !AudioPlayer.shared.player.isPlaying) {
                        if((playlistPlaying.count) > (rowPlaying! + 1)) {
                            playSound(playlistPlaying[rowPlaying! + 1])
                            if let sharedPlaylistsVC = mainTabBarCntInstantiate.playlistsVC as? PlaylistsVC {
                                let indexPathDeselect = IndexPath(row: rowPlaying!, section: 0)
                                sharedPlaylistsVC.mainTableView.deselectRow(at: indexPathDeselect, animated: true)
                                let indexPathSelect = IndexPath(row: rowPlaying! + 1, section: 0)
                                sharedPlaylistsVC.mainTableView.selectRow(at: indexPathSelect, animated: true, scrollPosition: .none)
                                sharedPlaylistsVC.selectedIndexPath = indexPathDeselect
                            }
                            rowPlaying! += 1
                        } else {
                            playSound(playlistPlaying[0])
                            rowPlaying = 0
                        }
                    }
                case 2: // repeat one
                    if(valueOfSlide.value == 0.0 && !AudioPlayer.shared.player.isPlaying) {
                        AudioPlayer.shared.player.play()
                    }
                default:
                    playButton.isHidden = false
                    playButtonMiniPlayer.isHidden = false
                    pauseButton.isHidden = true
                    pauseButtonMiniPlayer.isHidden = true
                    updateSliderTime.invalidate()
                }
            }
        } else if(AudioPlayer.shared.audioOnlineURL != nil) {
            valueOfSlide.value = Float(AudioPlayer.shared.playerOnline.currentItem!.currentTime().seconds/AudioPlayer.shared.playerOnline.currentItem!.duration.seconds)
            valueOfSlideMiniPlayer.value = valueOfSlide.value
            updatePassedTime(Double(AudioPlayer.shared.playerOnline.currentItem!.currentTime().seconds))
            if(Double(AudioPlayer.shared.playerOnline.currentItem!.duration.seconds) >= 0.0) {
                updateRemainedTime(Double(AudioPlayer.shared.playerOnline.currentItem!.duration.seconds), Double(AudioPlayer.shared.playerOnline.currentItem!.currentTime().seconds))
            } else {
                updateRemainedTime(0.0, Double(AudioPlayer.shared.playerOnline.currentItem!.currentTime().seconds))
            }
        }
    }
    
    func updatePassedTime(_ tiCur: TimeInterval) {
        let tiCur = ceil(tiCur)
        let seconds: Int = (Int(tiCur) % 60)
        let minutes: Int = (Int((tiCur / 60)) % 60)
        let hours: Int = (Int(tiCur / 3600))
        if(hours == 0){
            passedTimeTextView.text = String(NSString(format: "%d:%0.2d",minutes,seconds))
        } else {
            passedTimeTextView.text = String(NSString(format: "%d:%0.2d:%0.2d",hours,minutes,seconds))
        }
    }
    
    func updateRemainedTime(_ tiDur: TimeInterval, _ tiCur: TimeInterval) {
        let ti = ceil(tiDur) - ceil(tiCur)
        let seconds: Int = (Int(ti) % 60)
        let minutes: Int = (Int((ti / 60)) % 60)
        let hours: Int = (Int(ti / 3600))
        if(hours == 0){
            remainedTimeTextView.text = String(NSString(format: "-%d:%0.2d",minutes,seconds))
        } else {
            remainedTimeTextView.text = String(NSString(format: "-%d:%0.2d:%0.2d",hours,minutes,seconds))
        }
    }
    
    func imageCoverIsPlaying() {
        coverShadowImage.layer.shadowColor = UIColor.black.cgColor
        coverShadowImage.layer.shadowOpacity = 1
        coverShadowImage.layer.shadowOffset = CGSize.zero
        coverShadowImage.layer.shadowRadius = 10
    }
    
    func imageCoverIsNotPlaying() {
        coverShadowImage.layer.shadowColor = UIColor.black.cgColor
        coverShadowImage.layer.shadowOpacity = 0
        coverShadowImage.layer.shadowOffset = CGSize.zero
        coverShadowImage.layer.shadowRadius = 0
    }
    
    func blurEffectInBackground() {
        let backView = UIView(frame: defaultCoverBlur.bounds)
        backView.backgroundColor = UIColor(red: 100/255, green: 100/255, blue: 100/255, alpha: 0.6)
        defaultCoverBlur.addSubview(backView)

        
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = defaultCoverBlur.bounds
        defaultCoverBlur.addSubview(blurredEffectView)
    }
    
    func updateNowPlayingInfoCenter(_ image: UIImage) {
        if(AudioPlayer.shared.audioURL != nil) {
            becomeFirstResponder()
            let mediaArtwork = MPMediaItemArtwork(boundsSize: image.size) { (size: CGSize) -> UIImage in return image }
            
            MPNowPlayingInfoCenter.default().nowPlayingInfo = [
                MPMediaItemPropertyArtist : self.artistMusic,
                MPMediaItemPropertyTitle : self.titleMusic,
                MPMediaItemPropertyPlaybackDuration : AudioPlayer.shared.player.duration,
                MPMediaItemPropertyArtwork : mediaArtwork,
                MPNowPlayingInfoPropertyElapsedPlaybackTime : AudioPlayer.shared.player.currentTime,
            ]
            UIApplication.shared.beginReceivingRemoteControlEvents()
        } else if(AudioPlayer.shared.audioOnlineURL != nil) {
            becomeFirstResponder()
            let mediaArtwork = MPMediaItemArtwork(boundsSize: image.size) { (size: CGSize) -> UIImage in return image }
            
            MPNowPlayingInfoCenter.default().nowPlayingInfo = [
                MPMediaItemPropertyArtist : self.artistMusic,
                MPMediaItemPropertyTitle : self.titleMusic,
                MPMediaItemPropertyPlaybackDuration : CMTimeGetSeconds(AudioPlayer.shared.playerOnline.currentItem!.duration),
                MPMediaItemPropertyArtwork : mediaArtwork,
                MPNowPlayingInfoPropertyElapsedPlaybackTime : CMTimeGetSeconds(AudioPlayer.shared.playerOnline.currentItem!.currentTime()),
            ]
            UIApplication.shared.beginReceivingRemoteControlEvents()
        }
    }
    
    func swipeShowAndHide() {
        if(isOnTopMiniPlayer) {
            self.isOnTopMiniPlayer = false
            UIView.transition(with: view, duration: duration_for_slide_media_player, options: .transitionCrossDissolve, animations: {
//                self.miniPlayerView.alpha = 1.0
                UIApplication.shared.statusBarStyle = .lightContent
            })
        } else {
            self.isOnTopMiniPlayer = true
            UIView.transition(with: view, duration: duration_for_slide_media_player, options: .transitionCrossDissolve, animations: {
//                self.miniPlayerView.alpha = 0.0
                UIApplication.shared.statusBarStyle = .default
            })
        }
    }
    
    
    // IBActions
    @IBAction func playButton(_ sender: Any) {
        //updateSliderTime.invalidate()
        if(AudioPlayer.shared.audioURL != nil || AudioPlayer.shared.audioOnlineURL != nil) {
            AudioPlayer.shared.play()
            imageCoverIsPlaying()
            updateSliderTime = updateSliderTimeStart()
            playButton.isHidden = true
            playButtonMiniPlayer.isHidden = true
            pauseButton.isHidden = false
            pauseButtonMiniPlayer.isHidden = false
        }
    }
    @IBAction func pauseButton(_ sender: Any) {
        if(AudioPlayer.shared.audioURL != nil || AudioPlayer.shared.audioOnlineURL != nil) {
            AudioPlayer.shared.stop()
            imageCoverIsNotPlaying()
            //currentTime = AudioPlayer.shared.player.currentTime
            updateSliderTime.invalidate()
            playButton.isHidden = false
            playButtonMiniPlayer.isHidden = false
            pauseButton.isHidden = true
            pauseButtonMiniPlayer.isHidden = true
        }
    }
    func stopButton() {
        if(AudioPlayer.shared.audioURL != nil || AudioPlayer.shared.audioOnlineURL != nil) {
            if(AudioPlayer.shared.player.isPlaying) {
                AudioPlayer.shared.pause()
                imageCoverIsNotPlaying()
                //currentTime = AudioPlayer.shared.player.currentTime
                updateSliderTime.invalidate()
                playButton.isHidden = false
                playButtonMiniPlayer.isHidden = false
                pauseButton.isHidden = true
                pauseButtonMiniPlayer.isHidden = true
            }
        }
    }
    @IBAction func repeatButton(_ sender: Any) {
        repeatButtonValue += 1
        if(repeatButtonValue > 2) { repeatButtonValue = 0 }
        switchInRepeatButtonValue()
    }
    func switchInRepeatButtonValue() {
        switch repeatButtonValue {
        case 0:
            repeatButton.setImage(UIImage(named: "002-replay"), for: .normal)
        case 1:
            repeatButton.setImage(UIImage(named: "repeatBlack"), for: .normal)
        case 2:
            repeatButton.setImage(UIImage(named: "repeatBlackOnce"), for: .normal)
        default:
            repeatButtonValue = 0
            repeatButton.setImage(UIImage(named: "002-replay"), for: .normal)
        }
        UserDefaults.standard.set(repeatButtonValue, forKey: "repeatButtonValue")
    }
    
    @IBAction func suffleButton(_ sender: Any) {
        shuffleButtonValue += 1
        if(shuffleButtonValue > 1) { shuffleButtonValue = 0 }
        switchInSuffleButtonValue()
    }
    func switchInSuffleButtonValue() {
        switch shuffleButtonValue {
        case 0:
            shuffleButton.setImage(UIImage(named: "006-shuffle"), for: .normal)
            shuffleButton.backgroundColor = UIColor.white.withAlphaComponent(0)
        case 1:
            shuffleButton.setImage(UIImage(named: "006-shuffle"), for: .normal)
            shuffleButton.backgroundColor = color_dark_orange
        default:
            shuffleButtonValue = 0
            shuffleButton.setImage(UIImage(named: "006-shuffle"), for: .normal)
            shuffleButton.backgroundColor = UIColor.white.withAlphaComponent(0)
        }
        UserDefaults.standard.set(shuffleButtonValue, forKey: "shuffleButtonValue")
    }
    
    var isPlayingTemp = false
    @IBAction func sliderChangingTouchDown(_ sender: Any) {
        if(AudioPlayer.shared.audioURL != nil) {
            updateSliderTime.invalidate()
            if(AudioPlayer.shared.player.isPlaying) {
                isPlayingTemp = true
                AudioPlayer.shared.player.pause()
                imageCoverIsNotPlaying()
            }
        } else if(AudioPlayer.shared.audioOnlineURL != nil && AudioPlayer.shared.playerOnline.status == .readyToPlay) {
            updateSliderTime.invalidate()
            if(true) {
                isPlayingTemp = true
                AudioPlayer.shared.playerOnline.pause()
                imageCoverIsNotPlaying()
            }
        }
        //print("First tap")
    }
    @IBAction func sliderChangingCancelOrExit(_ sender: Any) {
        if(AudioPlayer.shared.audioURL != nil) {
            updateSliderTime = updateSliderTimeStart()
            AudioPlayer.shared.player.currentTime = AudioPlayer.shared.player.duration * Double(valueOfSlide.value)
            if(isPlayingTemp) {
                AudioPlayer.shared.player.play()
                imageCoverIsPlaying()
                isPlayingTemp = false
            }
        } else if(AudioPlayer.shared.audioOnlineURL != nil && AudioPlayer.shared.playerOnline.status == .readyToPlay) {
            updateSliderTime = updateSliderTimeStart()
            let seconds : Int64 = Int64(AudioPlayer.shared.playerOnline.currentItem!.duration.seconds * Double(valueOfSlide.value))
            let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
            AudioPlayer.shared.playerOnline.currentItem!.seek(to: targetTime)
            if(isPlayingTemp) {
                AudioPlayer.shared.playerOnline.play()
                imageCoverIsPlaying()
                isPlayingTemp = false
            }
        }
    }
    @IBAction func sliderChangingTouchUpInside(_ sender: Any) {
        if(AudioPlayer.shared.audioURL != nil) {
            updateSliderTime = updateSliderTimeStart()
            AudioPlayer.shared.player.currentTime = AudioPlayer.shared.player.duration * Double(valueOfSlide.value)
            if(isPlayingTemp) {
                AudioPlayer.shared.player.play()
                imageCoverIsPlaying()
                isPlayingTemp = false
            }
        } else if(AudioPlayer.shared.audioOnlineURL != nil && AudioPlayer.shared.playerOnline.status == .readyToPlay) {
            updateSliderTime = updateSliderTimeStart()
            let seconds : Int64 = Int64(AudioPlayer.shared.playerOnline.currentItem!.duration.seconds * Double(valueOfSlide.value))
            let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
            AudioPlayer.shared.playerOnline.currentItem!.seek(to: targetTime)
            if(isPlayingTemp) {
                AudioPlayer.shared.playerOnline.play()
                imageCoverIsPlaying()
                isPlayingTemp = false
            }
        }
        //print("Drag down")
    }
    @IBAction func sliderChangingValueChanged(_ sender: UISlider) {
        if(AudioPlayer.shared.audioURL != nil) {
            updatePassedTime(AudioPlayer.shared.player.duration * Double(valueOfSlide.value))
            updateRemainedTime(AudioPlayer.shared.player.duration, AudioPlayer.shared.player.duration * Double(valueOfSlide.value))
            //print("Zmieniaaaaaaam")
        } else if(AudioPlayer.shared.audioOnlineURL != nil && AudioPlayer.shared.playerOnline.status == .readyToPlay) {
            updatePassedTime(Double(AudioPlayer.shared.playerOnline.currentItem!.duration.seconds) * Double(valueOfSlide.value))
            updateRemainedTime(Double(AudioPlayer.shared.playerOnline.currentItem!.duration.seconds), Double(AudioPlayer.shared.playerOnline.currentItem!.duration.seconds) * Double(valueOfSlide.value))
        }
    }
    
    @IBAction func nextTrackButtonPressed(_ sender: Any) {
        if(AudioPlayer.shared.audioURL != nil) {
            if(rowPlaying != nil && playlistPlaying.count > 0) {
                if(rowPlaying! + 1 >= 0 && (playlistPlaying.count) > rowPlaying! + 1) {
                    playSound(playlistPlaying[rowPlaying! + 1])
                    //lol
                    if let tempPlaylistsVC = mainTabBarCntInstantiate.playlistsVC as? PlaylistsVC {
                        let indexPathDeselect = IndexPath(row: rowPlaying!, section: 0)
                        tempPlaylistsVC.mainTableView.deselectRow(at: indexPathDeselect, animated: true)
                        let indexPathSelect = IndexPath(row: rowPlaying! + 1, section: 0)
                        tempPlaylistsVC.mainTableView.selectRow(at: indexPathSelect, animated: true, scrollPosition: .none)
                        tempPlaylistsVC.selectedIndexPath = indexPathDeselect
                        print("wykonano")
                    }
                    
                    rowPlaying! += 1
                }
            }
        } else if(AudioPlayer.shared.audioOnlineURL != nil) {
            if(rowPlaying != nil && playlistPlayingOnline.count > 0) {
                if(rowPlaying! + 1 >= 0 && (playlistPlayingOnline.count) > rowPlaying! + 1) {
                    playSoundOnline(playlistPlayingOnline[rowPlaying! + 1])
                    if let tempOnlineListVC = mainTabBarCntInstantiate.onlineListVC as? OnlineListVC  {
                        let indexPathDeselect = IndexPath(row: rowPlaying!, section: 0)
                        tempOnlineListVC.musicTableView.deselectRow(at: indexPathDeselect, animated: true)
                        let indexPathSelect = IndexPath(row: rowPlaying! + 1, section: 0)
                        tempOnlineListVC.musicTableView.selectRow(at: indexPathSelect, animated: true, scrollPosition: .none)
                        tempOnlineListVC.selectedIndexPath = indexPathDeselect
                    }
                    
                    rowPlaying! += 1
                }
            }
        }
    }
    @IBAction func backTrackButtonPressed(_ sender: Any) {
        if(AudioPlayer.shared.audioURL != nil) {
            if(rowPlaying != nil && playlistPlaying.count > 0) {
                if(rowPlaying! != 0 && (playlistPlaying.count) >= rowPlaying! + 1) {
                    playSound(playlistPlaying[rowPlaying! - 1])
                    
                    if let sharedPlaylistsVC = mainTabBarCntInstantiate.playlistsVC as? PlaylistsVC {
                        let indexPathDeselect = IndexPath(row: rowPlaying!, section: 0)
                        sharedPlaylistsVC.mainTableView.deselectRow(at: indexPathDeselect, animated: true)
                        let indexPathSelect = IndexPath(row: rowPlaying! - 1, section: 0)
                        sharedPlaylistsVC.mainTableView.selectRow(at: indexPathSelect, animated: true, scrollPosition: .none)
                        sharedPlaylistsVC.selectedIndexPath = indexPathDeselect
                    }
                    
                    rowPlaying! -= 1
                }
            }
        } else if(AudioPlayer.shared.audioOnlineURL != nil) {
            if(rowPlaying != nil && playlistPlayingOnline.count > 0) {
                if(rowPlaying! != 0 && (playlistPlayingOnline.count) >= rowPlaying! + 1) {
                    playSoundOnline(playlistPlayingOnline[rowPlaying! - 1])
                    
                    if let tempOnlineListVC = mainTabBarCntInstantiate.onlineListVC as? OnlineListVC  {
                        let indexPathDeselect = IndexPath(row: rowPlaying!, section: 0)
                        tempOnlineListVC.musicTableView.deselectRow(at: indexPathDeselect, animated: true)
                        let indexPathSelect = IndexPath(row: rowPlaying! - 1, section: 0)
                        tempOnlineListVC.musicTableView.selectRow(at: indexPathSelect, animated: true, scrollPosition: .none)
                        tempOnlineListVC.selectedIndexPath = indexPathDeselect
                    }
                    
                    rowPlaying! -= 1
                }
            }
        }
    }

    @IBAction func allGestureRecognizerForMiniPlayer(_ status: Any) {
        masterVCInstantiate.miniMediaPlayerGesture(status)
    }
}
