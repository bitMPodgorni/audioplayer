//
//  LogginVC.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 22/03/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class LoginVC: UIViewController, UITextFieldDelegate {
    
    static var shared = UIViewController()
    
    @IBOutlet weak var userEmailTxt: UITextField!
    @IBOutlet weak var userPassTxt: UITextField!
    @IBOutlet weak var userMessageTxt: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var zalogujBtn: UIButton!
    
    let URL_USER_LOGIN = "https://wsbaplikacja.prv.pl/wsbapi/login.php"
    
    override func viewWillAppear(_ animated: Bool) {
        createGradientLayer()
        closeBtn.layer.cornerRadius = 17
        zalogujBtn.layer.cornerRadius = 10
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        LoginVC.shared = self
        
        UIApplication.shared.statusBarStyle = .default
        userEmailTxt.delegate = self
        userPassTxt.delegate = self
        
        self.hideKeyboardWhenTappedAround()
    }
    
    func createGradientLayer() {
        view.backgroundColor = UIColor.clear
        var backgroundLayer: CAGradientLayer!
        backgroundLayer = CAGradientLayer()
        backgroundLayer.frame = view.frame
        let colorTop = UIColor(red: 212.0 / 255.0, green: 228.0 / 255.0, blue: 239.0 / 255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 134.0 / 255.0, green: 174.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0).cgColor
        backgroundLayer.colors = [colorTop, colorBottom]
        backgroundLayer.startPoint = CGPoint(x: 1, y: 1)
        backgroundLayer.endPoint = CGPoint(x:0, y:0)
        view.layer.insertSublayer(backgroundLayer, at: 0)
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        guard let email = userEmailTxt.text else { return }
        guard let password = userPassTxt.text else { return }
        
        if(email.count >= 6 && password.count >= 8) {
            let headers = [
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            let postData = NSMutableData(data: "user_name=\(email)&user_pass=\(password)".data(using: String.Encoding.utf8)!)
            let request = NSMutableURLRequest(url: NSURL(string: URL_USER_LOGIN)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
            
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            URLSession.shared.dataTask(with: request as URLRequest) { (data, response, err) in
                guard let data = data else { return }
                let dataAsString = String(data: data, encoding: .utf8)
                if(dataAsString?.prefix(1) == "0") {
                    print("niezalogowano")
                    DispatchQueue.main.async {
                        self.userMessageTxt.text = "Błędne logowanie, popraw email lub hasło!"
                    }
                } else if(dataAsString?.prefix(1) == "1") {
                    print("zalogowano")
                    DispatchQueue.main.async {
                        UserDefaults.standard.set(true, forKey: "isLoggedIn")
                        UserDefaults.standard.set("\(email)", forKey: "userEmail")
                        LoggerSwitcher.updateRootVC()
                    }
                }
                }.resume()
        } else {
            userMessageTxt.text = "Zbyt krótki adres email i/lub hasło!"
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == userEmailTxt) {
            userPassTxt.becomeFirstResponder()
        } else if(textField == userPassTxt) {
            dismissKeyboard()
            loginButtonPressed(self)
        }
        return true
    }
    
}

extension LoginVC {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
