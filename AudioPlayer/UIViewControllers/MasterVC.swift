//
//  MasterVC.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 08/03/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class MasterVC: UIViewController, UITabBarDelegate {
    
    var myTabBarCnt = MainTabBarCnt()
    var isSwipeTmp = true
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaPlayerContainerView: UIView!
    @IBOutlet weak var buttonsContainerView: UIView!
    //@IBOutlet var buttons: [UIButton]!
    @IBOutlet var myTabBar: UITabBar!
    @IBOutlet weak var slidePositionMediaPlayerVC: NSLayoutConstraint!
    @IBOutlet weak var slidePositionButtons: NSLayoutConstraint!
    @IBOutlet weak var heightButtonsContainer: NSLayoutConstraint!
    @IBOutlet weak var mathView: UIView!
    
    static var shared = UIViewController()
    var playlistsVC = UIViewController()
    var onlineListVC = UIViewController()
    var masterNVC = UIViewController()
    
    var startUPandDOWN = false
    var startLEFTandRIGHT = false
    
    var menuIsToggle = true
    var selectedIndexTag = 0
    var mediaPlayerAboveSize: CGFloat = 0.0
    var sizeToBottom: CGFloat = 0.0
    let speedOfFingerSlide: CGFloat = 1.7
    var bottomButtonsAndMiniPlayer: CGFloat = 96.0
    
    var isShowMiniPlayer = false
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createTabBarController()
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        // Position of miniplayer on this and others devices! - v.important
        sizeToBottom = self.view.frame.height - (mathView.frame.size.height + mathView.frame.origin.y)
        slidePositionButtons.constant -= sizeToBottom
        heightButtonsContainer.constant += sizeToBottom
        bottomButtonsAndMiniPlayer = 96 + sizeToBottom
        slidePositionMediaPlayerVC.constant = self.mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer + 49
        slidePositionButtons.constant = 0
        contentViewBottomConstraint.constant -= 47
        
    }
    
    var mediaPlayerVCInstantiate: MediaPlayerVC?
    var mainTabBarCntInstantiate: MainTabBarCnt?
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "MediaPlayerSegue") {
            mediaPlayerVCInstantiate = segue.destination as? MediaPlayerVC
            mediaPlayerVCInstantiate?.masterVCInstantiate = self
            mediaPlayerVCInstantiate?.mainTabBarCntInstantiate = myTabBarCnt
        } else if (segue.identifier == "MainTabBarCntSegue") {
            mainTabBarCntInstantiate = segue.destination as? MainTabBarCnt
//            mainTabBarCntInstantiate?.masterVCInstantiate = self
        }
    }
    
    func createTabBarController() {
        
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//
        MasterVC.shared = self
//        playlistsVC = storyboard.instantiateViewController(withIdentifier: "PlaylistVC")
//        onlineListVC = storyboard.instantiateViewController(withIdentifier: "OnlineListVC")
//        masterNVC = storyboard.instantiateViewController(withIdentifier: "MasterNVC")
//
//        myTabBarCnt.viewControllers = [playlistsVC, onlineListVC, masterNVC]
        
        myTabBarCnt.view.frame = contentView.bounds
        contentView.addSubview(myTabBarCnt.view)
        
        myTabBar.delegate = self
        myTabBar.selectedItem = myTabBar.items![0]
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            if(selectedIndexTag == 0) {
                if let tempPlaylistVC = myTabBarCnt.playlistsVC as? PlaylistsVC {
                    tempPlaylistVC.scrollToTop()
                }
            }
            myTabBarCnt.selectedIndex = 0
            selectedIndexTag = 0
            break
        case 1:
            if(selectedIndexTag == 1) {
                if let tempOnlineListVC = myTabBarCnt.onlineListVC as? OnlineListVC {
                    tempOnlineListVC.jsonMyFunc()
                }
            }
            myTabBarCnt.selectedIndex = 1
            selectedIndexTag = 1
            break
        case 2:
            if(selectedIndexTag == 2) {
                if let tempMasterNVC = myTabBarCnt.masterNVC as? MasterNVC {
                    tempMasterNVC.popToRootViewController(animated: true)
                }
            }
            myTabBarCnt.selectedIndex = 2
            selectedIndexTag = 2
            break
        default:
            myTabBarCnt.selectedIndex = 0
            selectedIndexTag = 0
            break
        }
    }
    
    func firstMiniPlayerShow() {
        if(!isShowMiniPlayer) {
            slidePositionMediaPlayerVC.constant = self.mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer
            contentViewBottomConstraint.constant += 47
            isShowMiniPlayer = true
        }
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    func swipeShowAndHide() {
//        if let tempMediaPlayerVC = MediaPlayerVC.shared as? MediaPlayerVC {
//            tempMediaPlayerVC.swipeShowAndHide()
//        }
        mediaPlayerVCInstantiate?.swipeShowAndHide()
    }
    
    func miniMediaPlayerGesture(_ status: Any) {
        guard let tempMediaPlayerVC = mediaPlayerVCInstantiate else {return}
        
        func alphaGestureRecognizerMediaPlayer() {
            if (slidePositionMediaPlayerVC.constant >= mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer - 50 && slidePositionMediaPlayerVC.constant < mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer) {
                tempMediaPlayerVC.miniPlayerView.alpha = ((slidePositionMediaPlayerVC.constant - mediaPlayerContainerView.frame.height + bottomButtonsAndMiniPlayer) / 50) + 1
            } else if(slidePositionMediaPlayerVC.constant < mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer - 50) {
                tempMediaPlayerVC.miniPlayerView.alpha = 0
            } else if(slidePositionMediaPlayerVC.constant >= mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer) {
                tempMediaPlayerVC.miniPlayerView.alpha = 1
            }
        }
        
        if let status = status as? UIPanGestureRecognizer {
            let velocityY = status.velocity(in: self.view).y
            let velocityX = status.velocity(in: self.view).x
            
            if((abs(velocityX)>abs(velocityY) && !startUPandDOWN && (slidePositionMediaPlayerVC.constant == mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer /*|| slidePositionMediaPlayerVC.constant == 0*/)) || startLEFTandRIGHT) {
                startLEFTandRIGHT = true
                if(velocityX > 0) {
                    switch status.state {
                    case .began:
                        break
                    case .changed:
                        break
                    case .ended:
                        tempMediaPlayerVC.nextTrackButtonPressed(self)
                        startLEFTandRIGHT = false
                    case .cancelled, .failed, .possible:
                        print("failed, cancelled, possible")
                        startLEFTandRIGHT = false
                    default:
                        print("default")
                    }
                } else if(velocityX < 0) {
                    switch status.state {
                    case .began:
                        break
                    case .changed:
                        break
                    case .ended:
                        tempMediaPlayerVC.backTrackButtonPressed(self)
                        startLEFTandRIGHT = false
                    case .cancelled, .failed, .possible:
                        print("failed, cancelled, possible")
                        startLEFTandRIGHT = false
                    default:
                        print("default")
                    }
                } else if(velocityX == 0 && status.state == UIPanGestureRecognizer.State.ended) {
                    startLEFTandRIGHT = false
                }
            } else if((abs(velocityX)<abs(velocityY) && !startLEFTandRIGHT) || startUPandDOWN) {
                startUPandDOWN = true
                switch status.state {
                case .began:
                    if(slidePositionMediaPlayerVC.constant < 0) {
                        slidePositionMediaPlayerVC.constant = 0
                    } else if(slidePositionMediaPlayerVC.constant > mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer) {
                        slidePositionMediaPlayerVC.constant = mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer
                    }
                case .changed:
                    if(slidePositionMediaPlayerVC.constant >= 0 && slidePositionMediaPlayerVC.constant <= mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer) {
                        if(slidePositionMediaPlayerVC.constant < 0 || (slidePositionMediaPlayerVC.constant + (velocityY / 100) * speedOfFingerSlide) < 0) {
                            slidePositionMediaPlayerVC.constant = 0
                        } else if(slidePositionMediaPlayerVC.constant > mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer || (slidePositionMediaPlayerVC.constant + (velocityY / 100) * speedOfFingerSlide) > mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer) {
                            slidePositionMediaPlayerVC.constant = mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer
                        } else {
                            slidePositionMediaPlayerVC.constant += (velocityY / 100) * speedOfFingerSlide
                            slidePositionButtons.constant += (velocityY / 100) * speedOfFingerSlide / ((mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer) / buttonsContainerView.frame.height)
                            
                            //ALPHA
                            alphaGestureRecognizerMediaPlayer()
                            
                        }
                    }
                case .ended, .cancelled, .failed, .possible:
                    if(slidePositionMediaPlayerVC.constant > 100 && velocityY > 0 && !menuIsToggle) {
                        UIView.animate(withDuration: duration_for_toggle_media_player_animation, delay: 0.0, options: .curveEaseOut, animations: {
                            self.slidePositionMediaPlayerVC.constant = self.mediaPlayerContainerView.frame.height - self.bottomButtonsAndMiniPlayer
                            self.slidePositionButtons.constant = 0
                            tempMediaPlayerVC.miniPlayerView.alpha = 1
                            self.view.layoutIfNeeded()
                        }) { (success) in
                            self.swipeShowAndHide()
                            self.menuIsToggle = true
                        }
                    } else if(slidePositionMediaPlayerVC.constant < mediaPlayerContainerView.frame.height - bottomButtonsAndMiniPlayer - 100 && velocityY < 0 && menuIsToggle) {
                        UIView.animate(withDuration: duration_for_toggle_media_player_animation, delay: 0.0, options: .curveEaseOut, animations: {
                            self.slidePositionMediaPlayerVC.constant = 0
                            self.slidePositionButtons.constant = -49 - self.sizeToBottom
                            tempMediaPlayerVC.miniPlayerView.alpha = 0
                            self.view.layoutIfNeeded()
                        }) { (success) in
                            self.swipeShowAndHide()
                            self.menuIsToggle = false
                        }
                    } else {
                        if(((mediaPlayerContainerView.frame.height / 2.0) > slidePositionMediaPlayerVC.constant) && !menuIsToggle) {
                            //                            print("here we go now 1 - isToggleFalse! stay false")
                            UIView.animate(withDuration: duration_for_toggle_media_player_animation, delay: 0.0, options: .curveEaseOut, animations: {
                                self.slidePositionMediaPlayerVC.constant = 0
                                self.slidePositionButtons.constant = -49 - self.sizeToBottom
                                tempMediaPlayerVC.miniPlayerView.alpha = 0
                                self.view.layoutIfNeeded()
                            }) { (success) in
                                self.menuIsToggle = false
                            }
                        } else if(((mediaPlayerContainerView.frame.height / 2.0) <= slidePositionMediaPlayerVC.constant) && !menuIsToggle) {
                            //                            print("here we go now 2 - isToggleFalse! become true")
                            UIView.animate(withDuration: duration_for_toggle_media_player_animation, delay: 0.0, options: .curveEaseOut, animations: {
                                self.slidePositionMediaPlayerVC.constant =  self.mediaPlayerContainerView.frame.height - self.bottomButtonsAndMiniPlayer
                                self.slidePositionButtons.constant = 0
                                tempMediaPlayerVC.miniPlayerView.alpha = 1
                                self.view.layoutIfNeeded()
                            }) { (success) in
                                self.swipeShowAndHide()
                                self.menuIsToggle = true
                            }
                        } else if(((mediaPlayerContainerView.frame.height / 2.0) > slidePositionMediaPlayerVC.constant) && menuIsToggle) {
                            //                            print("here we go now 3 - isToggleTrue! become false")
                            UIView.animate(withDuration: duration_for_toggle_media_player_animation, delay: 0.0, options: .curveEaseOut, animations: {
                                self.slidePositionMediaPlayerVC.constant = 0
                                self.slidePositionButtons.constant = -49 - self.sizeToBottom
                                tempMediaPlayerVC.miniPlayerView.alpha = 0
                                self.view.layoutIfNeeded()
                            }) { (success) in
                                self.swipeShowAndHide()
                                self.menuIsToggle = false
                            }
                        } else if(((mediaPlayerContainerView.frame.height / 2.0) <= slidePositionMediaPlayerVC.constant) && menuIsToggle) {
                            //                            print("here we go now 4 - isToggleTrue! stay true")
                            UIView.animate(withDuration: duration_for_toggle_media_player_animation, delay: 0.0, options: .curveEaseOut, animations: {
                                self.slidePositionMediaPlayerVC.constant =  self.mediaPlayerContainerView.frame.height - self.bottomButtonsAndMiniPlayer
                                self.slidePositionButtons.constant = 0
                                tempMediaPlayerVC.miniPlayerView.alpha = 1
                                self.view.layoutIfNeeded()
                            }) { (success) in
                                self.menuIsToggle = true
                            }
                        } else {
                            print("toggleMediaPlayer — error")
                        }
                    }
                    startUPandDOWN = false
                default:
                    print("default")
                }
            } else {
                if(startLEFTandRIGHT) {
                    startLEFTandRIGHT = false
                } else if(startUPandDOWN) {
                    status.state = UIPanGestureRecognizer.State.changed
                    startUPandDOWN = false
                } else {
                    
                }
            }
        } else if let _ = status as? UITapGestureRecognizer {
            UIView.animate(withDuration: duration_for_toggle_media_player_animation, delay: 0.0, options: .curveEaseOut, animations: {
                self.slidePositionMediaPlayerVC.constant = 0
                self.slidePositionButtons.constant = -49 - self.sizeToBottom
                tempMediaPlayerVC.miniPlayerView.alpha = 0
                self.view.layoutIfNeeded()
            }) { (success) in
                self.swipeShowAndHide()
                self.menuIsToggle = false
            }
        }
    }
}
