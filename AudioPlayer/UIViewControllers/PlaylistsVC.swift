//
//  PlaylistsVC.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 28/02/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit

class PlaylistsVC: UIViewController {

    @IBOutlet var musicTableView: UITableView!
    @IBOutlet var mainTableView: UITableView!
    
    static var shared = UIViewController()
    
    var selectedIndexPath: IndexPath?
    
    var previousCell: MusicCell = MusicCell()
    
    var musics: [Music] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PlaylistsVC.shared = self
        
        musics = createArray()
        
        musicTableView.delegate = self
        musicTableView.dataSource = self
    }
    
    func createArray() -> [Music] {
        var tempMusics: [Music] = []

        let music1 = Music(fileName: "086-front-music.pl", fileExtension: "mp3", image: #imageLiteral(resourceName: "imaginedragon"), title: "Radioactive", artist: "Imagine Dragons")
        let music2 = Music(fileName: "152-front-music.pl", fileExtension: "mp3", image: #imageLiteral(resourceName: "leavesImage"), title: "Survive", artist: "Don Diablo ft. Emeli Sande & Gucci Mane")
        let music3 = Music(fileName: "155-front-music.pl", fileExtension: "mp3", image: #imageLiteral(resourceName: "videoProduction"), title: "Give It To Me", artist: "Timberland ft. N. Furtado and J. Timberlake")

        tempMusics.append(music1)
        tempMusics.append(music2)
        tempMusics.append(music3)

        return tempMusics
    }
}
extension PlaylistsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return musics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let music = musics[indexPath.row]

        let cell = tableView.dequeueReusableCell(withIdentifier: "MusicCell") as! MusicCell

        cell.setMusic(music: music)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let tempMediaPlayerVC = MediaPlayerVC.shared as? MediaPlayerVC {
             if(tempMediaPlayerVC.playlistPlaying == musics ) {
                tempMediaPlayerVC.playSound(tempMediaPlayerVC.playlistPlaying[indexPath.row])
                tempMediaPlayerVC.rowPlaying = indexPath.row
            } else {
                tempMediaPlayerVC.playSoundWith(music: musics[indexPath.row], musics: musics, row: indexPath.row)
            }
        }

        selectedIndexPath = indexPath
        if let tempMasterVC = MasterVC.shared as? MasterVC {
            tempMasterVC.firstMiniPlayerShow()
        }
    }
}
extension PlaylistsVC {
    func scrollToTop() {
        let indexPath = IndexPath(row: 0, section: 0)
        musicTableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
}
