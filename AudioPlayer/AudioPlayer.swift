//
//  AudioPlayer.swift
//  AudioPlayer
//
//  Created by Michał Podgórni on 23/03/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import Foundation
import ARKit

class AudioPlayer {
    
    static let shared = AudioPlayer()
    
    var player: AVAudioPlayer
    var playerOnline: AVPlayer
    
    init() {
        self.player = AVAudioPlayer()
        self.playerOnline = AVPlayer()
        playerOnline.automaticallyWaitsToMinimizeStalling = false
    }
    
    var audioURL: URL?
    var audioOnlineURL: URL?
    
    func playSoundWith(name: String, ext: String) {
        guard let audioPath = Bundle.main.url(forResource: name, withExtension: ext) else { return }
        audioURL = Bundle.main.url(forResource: name, withExtension: ext)
        do {
            player = try AVAudioPlayer.init(contentsOf: audioPath)
            player.prepareToPlay()
            player.play()
        } catch {
            print(error)
        }
    }
    
    func playSoundOnlineWith(localized: String) {
        guard let url = URL(string: localized) else { return }
        audioOnlineURL = url
        let playerItem = AVPlayerItem.init(url: url)
        playerOnline = AVPlayer.init(playerItem: playerItem)
        playerOnline.play()
    }
    
    func stop() {
        if(audioURL != nil) {
            if player.isPlaying {
                player.stop()
            }
        } else if(audioOnlineURL != nil) {
            playerOnline.pause()
        }
    }
    func pause() {
        if(audioURL != nil) {
            if (player.isPlaying) {
                player.pause()
            }
        } else if(audioOnlineURL != nil) {
            playerOnline.pause()
        }
    }
    func play() {
        if (audioURL != nil && !player.isPlaying) {
            player.play()
        } else if (audioOnlineURL != nil) {
            playerOnline.play()
        }
    }
}
